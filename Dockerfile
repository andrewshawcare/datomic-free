FROM centos:7

EXPOSE 4334

ENV DATOMIC_FREE_VERSION="0.9.5344"
ENV DATOMIC_FREE_HOME="/opt/datomic-free-${DATOMIC_FREE_VERSION}"
ENV PATH="${PATH}:${DATOMIC_FREE_HOME}/bin"

RUN yum --assumeyes --quiet install \
  java-1.8.0-openjdk-headless \
  unzip

RUN curl --silent --location --output /tmp/datomic-free.zip \
  https://my.datomic.com/downloads/free/${DATOMIC_FREE_VERSION} && \
  unzip -q -d /opt /tmp/datomic-free.zip && \
  rm /tmp/datomic-free.zip

RUN cp ${DATOMIC_FREE_HOME}/config/samples/free-transactor-template.properties /transactor.properties

ENTRYPOINT ["transactor"]
CMD ["/transactor.properties"]
